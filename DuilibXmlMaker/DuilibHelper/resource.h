﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 DuilibHelper.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DUILIBHELPER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_MAKE_XML             130
#define IDR_MENU1                       132
#define IDD_DIALOG_CHOICECTRL           133
#define IDD_DIALOG_PIC_CONFIG           135
#define IDC_LIST_ATTR_NAME              1000
#define IDC_LIST_ATTR_DESC              1001
#define IDC_BUTTON_NEW                  1002
#define IDC_BUTTON_OPEN                 1003
#define IDC_BUTTON_SHOW                 1003
#define IDC_BUTTON_OUTPUT               1004
#define IDC_EDIT_VALUE                  1005
#define IDC_BUTTON_UPDATE               1006
#define IDC_TREE_NODE                   1007
#define IDC_COMBO1                      1009
#define IDC_BUTTON_OK                   1010
#define IDC_STATIC_ATTR_NAME            1011
#define IDC_LIST_1                      1013
#define IDC_LIST2                       1014
#define IDC_LIST_2                      1014
#define IDC_CHECK1                      1015
#define IDC_CHECK2                      1016
#define IDC_EDIT1                       1017
#define IDC_EDIT2                       1018
#define IDC_EDIT3                       1019
#define IDC_EDIT4                       1020
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_INSERT                       32775
#define ID_DELETE                       32776
#define ID_UP                           32777
#define ID_DOWN                         32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
