﻿#pragma once

// CControlChoiceDlg 对话框

class CControlChoiceDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CControlChoiceDlg)

public:
	CControlChoiceDlg(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CControlChoiceDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_CHOICECTRL };
#endif

  CString choice_ctrl_name;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

  virtual BOOL OnInitDialog();

  afx_msg void OnBnClickedButtonOk();

public:

  CComboBox ctrl_combo_;

};
