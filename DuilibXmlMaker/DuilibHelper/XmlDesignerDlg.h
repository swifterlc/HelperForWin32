﻿#pragma once

// XmlDesignerDlg 对话框

class XmlDesignerDlg : public CDialogEx
{
	DECLARE_DYNAMIC(XmlDesignerDlg)

public:
	XmlDesignerDlg(CString loadFilePath = L"");

	virtual ~XmlDesignerDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_MAKE_XML };
#endif

  helper_lib::Events<std::wstring, std::wstring> on_attr_value_list_sel_;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

  virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

private:
  void LoadChildNode(HTREEITEM parent, xml_node& parent_node);

  void SetupUI();
  void SetSaveFileName();

  afx_msg void OnTreeClick(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnTreeRClick(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnTreeKeyDown(NMHDR* pNMHDR, LRESULT* pResult);

  afx_msg void OnMenuDelete();
  afx_msg void OnMenuMoveDown();
  afx_msg void OnMenuMoveUp();
  afx_msg void OnMenuInsertChild();
  afx_msg void OnXmlShow();
  afx_msg void OnXmlGenerate();

  afx_msg void OnAttrValueListClick(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnAttrValueListKillFocus(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnAttrValueListSetFocus(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnAttrValueUpdate();

  virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
  CString file_path_;

  xml_document doc_;

  map<HTREEITEM, xml_node> node_map;

  HTREEITEM pre_sel_item;

  int nSelItem; //用于CListCtrl一直选中高亮

  CTreeCtrl node_tree_;
  CListCtrl attr_desc_list_;
  CEdit value_edit_;
  CStatic name_label_;
  CButton update_btn_;
};
