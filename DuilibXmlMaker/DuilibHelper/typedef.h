#pragma once

typedef struct Attribute
{
  Attribute() {}
  Attribute(
    std::wstring const&inherit,
    std::wstring const&name,
    std::wstring const&default,
    std::wstring const&_type,
    std::wstring const&comment) :
    inherit_(inherit),
    name_(name),
    default_(default),
    type_(_type),
    comment_(comment) {}
  wstring inherit_;
  wstring name_;
  wstring default_;
  wstring type_;
  wstring comment_;
}Attribute, *LPAttribute;


using AttrMap = std::map<std::wstring, Attribute>;
using ControllerAttrMap = std::map<std::wstring, AttrMap>;


class ControllerConfigData
{
public:
  ControllerConfigData() = default;
  ControllerConfigData(
    BOOL is_longitudinal_,
    BOOL is_zoom_,
    int start_pos_,
    int pic_width_,
    int pic_height_,
    CString pic_name_,
    list<CString> attrs_,
    CString ctr_name_)
  {
    is_longitudinal = is_longitudinal_;
    is_zoom = is_zoom_;
    start_pos = start_pos_;
    pic_width = pic_width_;
    pic_height = pic_height_;
    pic_name = pic_name_;
    attrs = attrs_;
    ctr_name = ctr_name_;
  }

  ControllerConfigData& Next() { start_pos = start_pos + (is_longitudinal ? pic_width : pic_height); return *this; }

  void ImgAttrForEach(std::function<void(CString, CString)> fn)
  {
    int index = 0;
    for (auto const&img_attr_name : attrs) {
      CString value;
      int left = is_longitudinal ? start_pos : index * pic_width;
      int top = is_longitudinal ? index * pic_height : start_pos;
      value.Format(L"file='%s' source='%d,%d,%d,%d'",
        pic_name.GetString(),
        left,top,left + pic_width,top+pic_height);
      if (is_zoom) {
        //#TODO 9宫格缩放，需要详细制定
      }
      index++;
      fn(img_attr_name, value);
    }
  }

  BOOL is_longitudinal; //是否是纵向
  BOOL is_zoom; //是否是缩放
  int start_pos;  //起点位置
  int pic_width;  //图片单元宽
  int pic_height; //图片单元高
  CString pic_name; //图片名
  list<CString> attrs;  //img_attr_name_list
  CString ctr_name; //控件名称
};

using ConfigDataMap = std::map<std::wstring, ControllerConfigData>;