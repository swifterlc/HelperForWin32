#include "stdafx.h"
#include "application.h"
#include "main_frame.h"

int APIENTRY wWinMain(
  _In_ HINSTANCE ins,
  _In_opt_ HINSTANCE,
  _In_ LPWSTR    lpCmdLine,
  _In_ int       nCmdShow)
{
  UNREFERENCED_PARAMETER(lpCmdLine);
  UNREFERENCED_PARAMETER(nCmdShow);

  Application* app = Application::Instance();
  if (app) {
    if (app->Initialize(ins)) {
      app->Run(lpCmdLine);
      app->Uninitialize();
    }
    app->DestroyInstance();
    app = nullptr;
  }

  return 0;
}

namespace
{
  Application *g_application_ = nullptr;
}

Application::Application()
  :main_thread_id_(::GetCurrentThreadId())
{

}

Application::~Application()
{
}

Application* Application::Instance()
{
  return g_application_ ? g_application_ : (g_application_ = new(std::nothrow) Application);
}

void Application::DestroyInstance()
{
  if (g_application_) delete g_application_;
}

bool Application::Initialize(HINSTANCE inst)
{
  int num_args = 0;
  LPWSTR *szArgList = ::CommandLineToArgvW(::GetCommandLineW(), &num_args);
  if (num_args <= 1) {
    ::MessageBox(NULL,L"未检测到输入文件路径参数", L"error", MB_OK);
    return false;
  }

  file_path_ = szArgList[1];
  if (!fs::exists(file_path_) || !fs::is_regular_file(file_path_)) {
    ::MessageBox(NULL,L"输入参数正确,请输入合法xml文件路径", L"error", MB_OK);
  }

  try {
    file_path_ = fs::canonical(file_path_, GetModuleDir().c_str());
  } catch (...) {
    ::MessageBox(NULL, L"输入参数正确,请输入合法xml文件路径", L"error", MB_OK);
    return false;
  }

  // 更改当前控制台代码页为UTF8解决乱码问题
  ::SetConsoleOutputCP(65001);

  // Duilib
  CPaintManagerUI::SetInstance(inst);
  CPaintManagerUI::SetResourcePath(file_path_.parent_path().c_str());
  CPaintManagerUI::SetCurrentPath(CPaintManagerUI::GetInstancePath().GetData());

  // COM
  HRESULT hr = ::CoInitializeEx(NULL,
#if defined(WIN32) && !defined(UNDER_CE)
    COINIT_APARTMENTTHREADED
#else
    COINIT_MULTITHREADED
#endif
  );
  if (FAILED(hr)) return false;

  // this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
  ::DefWindowProc(NULL, 0, 0, 0L);

  // Ole
  if (FAILED(::OleInitialize(NULL))) return false;

  // ATL
  if (FAILED(module_.Init(NULL, inst))) return false;

  WCHAR module_dir[MAX_PATH]{};
  GetCurModuleDir(module_dir, MAX_PATH);
  module_dir_ = module_dir;

  // Locale
  std::locale::global(std::locale(""));

  // CWndShadow
  CWndShadow::Initialize(inst);

  //TODO other init

  return true;
}

bool Application::Run(LPCWSTR strCmdLine)
{
  //创建窗口
  main_frame_.reset(new MainFrame);
  main_frame_->SetSkinFileName(file_path_.filename().c_str());
  main_frame_->Create(NULL, _T("Previewer"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
  main_frame_->CenterWindow();
  main_frame_->ShowWindow();


  CPaintManagerUI::MessageLoop();
  CPaintManagerUI::Term();

  return true;
}

void Application::Uninitialize()
{
  module_.Term();
  ::OleUninitialize();
  ::CoUninitialize();

}

DWORD Application::GetMainThreadId() const
{
  return main_thread_id_;
}

BOOL Application::GetIsMainThread() const
{
  return main_thread_id_ == ::GetCurrentThreadId();
}

const std::wstring& Application::GetDataStorageDir() const
{
  return data_storage_directory_;
}

const std::wstring& Application::GetModuleDir() const
{
  return module_dir_;
}

HWND Application::GetMainFrameWnd() const
{
  return main_frame_ ? main_frame_->GetHWND() : NULL;
}
