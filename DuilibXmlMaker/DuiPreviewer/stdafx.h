﻿// stdafx.h: 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 项目特定的包含文件
//

#pragma once

#define WIN32_LEAN_AND_MEAN             // 从 Windows 头文件中排除极少使用的内容
// Windows 头文件
#include <windows.h>
#include <atlbase.h>
#include <tlhelp32.h>
#include <shellapi.h>

// C 运行时头文件
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// C++ 头文件
#include <memory>
#include <xlocale>
#include <functional>
#include <sstream>
#include <filesystem>
#include <map>
#ifdef _UNICODE
typedef std::wstring TString;
typedef std::wstringstream TStringStream;
#define to_TString to_wstring
#else
typedef std::string TString;
typedef std::stringstream TStringStream;
#define to_TString to_string
#endif // !_UNICODE
namespace fs = std::experimental::filesystem;

//#include "resource.h"

//common lib

// Duilib
#include "Duilib\header.h"

#include "application.h"

// TypeDef
#include "typedef.h"

#include "common.h"