#include "stdafx.h"
#include "main_frame.h"

MainFrame::MainFrame()
{

}

void MainFrame::SetSkinFileName(LPCWSTR file_name)
{
  skin_file_name_ = file_name;
}

void MainFrame::OnInitWnd()
{
  //AcceptDropFile();

  //控件绑定
  AsyncTask::Post([] {});
}

void MainFrame::OnFinalMessage(HWND hWnd)
{
  ::PostQuitMessage(0);
}

LRESULT MainFrame::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  if (uMsg == WM_KEYDOWN && wParam == VK_ESCAPE)Close();
  return __super::HandleMessage(uMsg, wParam, lParam);
}

LRESULT MainFrame::MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, bool& bHandled)
{
  //屏蔽双击标题栏最大化
  //if (uMsg == WM_NCLBUTTONDBLCLK) {
  //  bHandled = TRUE;
  //  return FALSE;
  //}
  return __super::MessageHandler(uMsg, wParam, lParam, bHandled);
}

void MainFrame::AcceptDropFile()
{
  ::DragAcceptFiles(m_hWnd, TRUE);

  ::ChangeWindowMessageFilter(WM_DROPFILES, MSGFLT_ADD);
  ::ChangeWindowMessageFilter(0x0049, MSGFLT_ADD);
}
