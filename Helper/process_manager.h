#pragma once
#include "common.h"
#include "string.h"
#include <string>
#include <functional>
#include <vector>
#include <windows.h>

#include "ensure_clean_up.h"

HelperLibBegin

class ProcessManager
{
public:
  using OtherCheckFunc = std::function<HANDLE()>;

  ProcessManager();
  ~ProcessManager();

  /**
   * 子进程启动参数初始化
   * param[in]  sub_process_path  子进程路径(支持相对当前进程的相对路径)
   * param[in]  process_cmdline 启动命令参数
   * param[in]  sub_process_workdir 指定子进程工作路径(默认设定子进程模块所在路径为工作路径)
   * return  void
   */
  void Init(LPCTSTR sub_process_path, LPCTSTR process_cmdline = nullptr, LPCTSTR sub_process_workdir = nullptr);

  /**
   * 提供方法检测已存子进程
   * param[in]  other_check_func std::function<HANDLE()> 查找成功返回进程句柄,后续被注入到manager成员中被管理
   * return
   */
  void SetOtherCheckFunc(OtherCheckFunc const&other_check_func);

  /**
   * 启动子进程,提供修改命令参数和工作路径的机会
   * param[in]  process_cmdline
   * param[in]  sub_process_workdir
   * return  启动成功返回true
   */
  bool LaunchSubProcess(LPCTSTR process_cmdline = nullptr, LPCTSTR sub_process_workdir = nullptr);

  /**
   * 结束子进程运行(一般在执行前做一次进程存在的检查)
   * param[in]  exit_code
   * return
   */
  void KillSubProcess(UINT exit_code = 0);

  /**
   * 检测进程是否启动
   * 如果当前保存的子进程句柄无效,则会调用 [已存进程检查] ,如果检查到已存进程，则注入到成员变量中
   * return  启动返回true
   */
  bool IsSubProcessExist();

  /**
   * 运行一个进程
   * param[in]  path 进程路径(支持相对路径)
   * param[in]  args 启动进程参数
   * param[in]  dir  指定进程运行工作目录(默认以被启动进程所在目录为工作目录)
   * return  进程句柄Handle,启动失败返回空
   */
  static HANDLE RunProcess(LPCTSTR path, LPCTSTR args = nullptr, LPCTSTR dir = nullptr);

  /**
   * 使用系统默认浏览器打开指定URL
   * param[in]  url
   * return  成功返回TRUE
   */
  static BOOL OpenUrl(LPCTSTR url);

  /**
   * 构造命令行
   * param[in]  cmdline_args [example:{"--name=zhangsan", "age=18",...}]
   * return  命令行字符串
   */
  static std::wstring MakeCmdline(std::vector<std::wstring> cmdline_args);

private:
  TString sub_process_path_{};  //管理子进程路径
  TString process_cmdline_{}; //管理子进程命令参数
  TString sub_process_workdir_{}; //子进程工作路径
  CEnsureCloseHandle sub_process_handle_{}; //子进程句柄

  OtherCheckFunc other_check_func_; //检查非manager启动已存在进程,返回值将被manager管理
};


HelperLibEnd