#include "process_manager.h"
#include <filesystem>

HelperLibBegin

ProcessManager::ProcessManager()
{

}


ProcessManager::~ProcessManager()
{

}

void ProcessManager::Init(LPCTSTR sub_process_path, LPCTSTR process_cmdline /*= nullptr*/, LPCTSTR sub_process_workdir /*= nullptr*/)
{
#define XX(x) if(x) x##_ = x;
  XX(sub_process_path);
  XX(process_cmdline);
  XX(sub_process_workdir);
#undef XX
}

void ProcessManager::SetOtherCheckFunc(OtherCheckFunc const&other_check_func)
{
  other_check_func_ = other_check_func;
}

bool ProcessManager::LaunchSubProcess(LPCTSTR process_cmdline /*= nullptr*/, LPCTSTR sub_process_workdir /*= nullptr*/)
{
  if (sub_process_path_.length() == 0) return false;
  HANDLE sub_process_handle = RunProcess(sub_process_path_.c_str(),
    process_cmdline ? process_cmdline : (process_cmdline_.length() > 0 ? process_cmdline_.c_str() : nullptr),
    sub_process_workdir ? sub_process_workdir : (sub_process_workdir_.length() > 0 ? sub_process_workdir_.c_str() : nullptr));
  if (sub_process_handle) {
    sub_process_handle_ = sub_process_handle;
  }

  return !!sub_process_handle;
}

void ProcessManager::KillSubProcess(UINT exit_code /*= 0*/)
{
  ::TerminateProcess(sub_process_handle_, exit_code);
}

bool ProcessManager::IsSubProcessExist()
{
  static auto check_process = [](HANDLE handle) {
    return handle ? ::WaitForSingleObject(handle, 0) == WAIT_TIMEOUT : false;
  };

  if (check_process(sub_process_handle_)) {
    return true;
  } else if (other_check_func_) {
    auto hd = other_check_func_();
    if (check_process(hd)) {
      sub_process_handle_ = hd;
      return true;
    }
    return false;
  }

  return false;
}

HANDLE ProcessManager::RunProcess(LPCTSTR path, LPCTSTR args /*= nullptr*/, LPCTSTR dir /*= nullptr*/)
{
  namespace fs = std::experimental::filesystem;

  //指定了工作目录,但工作目录参数错误,则不启动进程
  if (dir && (!fs::exists(dir) || !fs::is_directory(dir))) {
    return nullptr;
  }

  auto p = fs::path(path);

  std::wstring _dir = p.parent_path();
  SHELLEXECUTEINFO execute_info;
  execute_info.cbSize = sizeof(SHELLEXECUTEINFO);
  execute_info.fMask = SEE_MASK_NOCLOSEPROCESS;
  execute_info.hwnd = NULL;
  execute_info.lpVerb = NULL;
  execute_info.lpFile = p.c_str();
  execute_info.lpParameters = args;
  if (dir != nullptr) {
    execute_info.lpDirectory = dir;
  } else {
    execute_info.lpDirectory = _dir.c_str();
  }
  execute_info.nShow = SW_SHOWNORMAL;
  execute_info.hInstApp = NULL;

  return !!::ShellExecuteEx(&execute_info) ? execute_info.hProcess : nullptr;
}

BOOL ProcessManager::OpenUrl(LPCTSTR url)
{
  SHELLEXECUTEINFO execute_info;

  memset(&execute_info, 0, sizeof(execute_info));
  execute_info.cbSize = sizeof(execute_info);
  execute_info.hwnd = NULL;
  execute_info.lpVerb = L"open";
  execute_info.lpFile = url;
  execute_info.nShow = SW_SHOWNORMAL;
  execute_info.fMask = SEE_MASK_NOCLOSEPROCESS;

  return ::ShellExecuteEx(&execute_info);
}

std::wstring ProcessManager::MakeCmdline(std::vector<std::wstring> cmdline_args)
{
  std::wstring ret{};
  std::for_each(cmdline_args.begin(), cmdline_args.end(), [&](auto const&arg) {
    ret.append(arg.c_str());
    ret.append(L" ");
  });

  if (ret[ret.length() - 1] == ' ') {
    ret = std::move(ret.substr(0, ret.length() - 1));
  }

  return std::move(ret);
}

HelperLibEnd