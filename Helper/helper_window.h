#pragma once
#include <windows.h>
#include <windowsx.h>
#include "common.h"
#include <assert.h>

HelperLibBegin

//摘自 Duilib CWindowWnd
//win32窗口基本封装

#define GET_WND() auto _wnd = static_cast<T const*>(this)->GetHWND()

template<typename T>
class THelperWindow
{
public:
  RECT GetClientRectHelper() const
  {
    GET_WND();
    RECT rect{};
    ::GetClientRect(_wnd, &rect);
    return rect;
  }

  RECT GetClientRectForScreenHelper() const
  {
    GET_WND();
    RECT rect = GetClientRectHelper();
    ::ClientToScreen(_wnd, (LPPOINT)(&rect));
    ::ClientToScreen(_wnd, (LPPOINT)(&rect) + 1);
    return rect;
  }

  POINT GetPosHelper() const
  {
    GET_WND();
    RECT rec = GetClientRectForScreenHelper();
    return *(LPPOINT)(&rec);
  }

  void SetWindowPosHelper(HWND insert_after, RECT rect, UINT uFlag) const
  {
    GET_WND();
    ::SetWindowPos(
      _wnd, 
      insert_after, 
      rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 
      uFlag);
  }

  bool IsWindowVisibleHelper() const
  {
    GET_WND();
    return !!::IsWindowVisible(_wnd);
  }

  DWORD GetWindowTypeHelper() const
  {
    GET_WND();
    assert(::IsWindow(_wnd));
    DWORD dwStyle = ::GetWindowLong(_wnd, GWL_STYLE);
    if (dwStyle&WS_CHILD)return WS_CHILD;
    if (dwStyle&WS_POPUP)return WS_POPUP;
    return WS_OVERLAPPED;
  }

  void ShowWindowHelper(bool show = true, bool take_focus = true) const
  {
    GET_WND();
    assert(::IsWindow(_wnd));
    if (!::IsWindow(_wnd)) return;
    ::ShowWindow(_wnd, show ? (take_focus ? SW_SHOWNORMAL : SW_SHOWNOACTIVATE) : SW_HIDE);
  }

  void CloseHelper(UINT ret = IDOK) const
  {
    GET_WND();
    assert(::IsWindow(_wnd));
    if (!::IsWindow(_wnd)) return;
    ::PostMessage(_wnd, WM_CLOSE, (WPARAM)ret, 0L);
  }

  void CenterWindowHelper() const
  {
    GET_WND();
    assert(::IsWindow(_wnd));
    assert((GetWindowStyle(_wnd)&WS_CHILD) == 0);
    RECT rcDlg = { 0 };
    ::GetWindowRect(_wnd, &rcDlg);
    RECT rcArea = { 0 };
    RECT rcCenter = { 0 };
    HWND hWnd = _wnd;
    HWND hWndParent = ::GetParent(_wnd);
    HWND hWndCenter = ::GetWindowOwner(_wnd);
    if (hWndCenter != NULL)
      hWnd = hWndCenter;

    // 处理多显示器模式下屏幕居中
    MONITORINFO oMonitor = {};
    oMonitor.cbSize = sizeof(oMonitor);
    ::GetMonitorInfo(::MonitorFromWindow(_wnd, MONITOR_DEFAULTTONEAREST), &oMonitor);
    rcArea = oMonitor.rcWork;

    if (hWndCenter == NULL || IsIconic(hWndCenter))
      rcCenter = rcArea;
    else
      ::GetWindowRect(hWndCenter, &rcCenter);

    int DlgWidth = rcDlg.right - rcDlg.left;
    int DlgHeight = rcDlg.bottom - rcDlg.top;

    // Find dialog's upper left based on rcCenter
    int xLeft = (rcCenter.left + rcCenter.right) / 2 - DlgWidth / 2;
    int yTop = (rcCenter.top + rcCenter.bottom) / 2 - DlgHeight / 2;

    // The dialog is outside the screen, move it inside
    if (xLeft < rcArea.left) xLeft = rcArea.left;
    else if (xLeft + DlgWidth > rcArea.right) xLeft = rcArea.right - DlgWidth;
    if (yTop < rcArea.top) yTop = rcArea.top;
    else if (yTop + DlgHeight > rcArea.bottom) yTop = rcArea.bottom - DlgHeight;
    ::SetWindowPos(_wnd, NULL, xLeft, yTop, -1, -1, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
  }

  LRESULT SendMessageHelper(UINT uMsg, WPARAM wParam = 0, LPARAM lParam = 0L) const
  {
    GET_WND();
    assert(::IsWindow(_wnd));
    return ::SendMessage(_wnd, uMsg, wParam, lParam);
  }

  LRESULT PostMessageHelper(UINT uMsg, WPARAM wParam = 0, LPARAM lParam = 0L) const
  {
    GET_WND();
    assert(::IsWindow(_wnd));
    return ::PostMessage(_wnd, uMsg, wParam, lParam);
  }

  void ResizeClientHelper(int cx = -1, int cy = -1) const
  {
    GET_WND();
    assert(::IsWindow(_wnd));
    RECT rc = { 0 };
    if (!::GetClientRect(_wnd, &rc)) return;
    if (cx != -1) rc.right = cx;
    if (cy != -1) rc.bottom = cy;
    if (!::AdjustWindowRectEx(&rc, GetWindowStyle(_wnd), (!(GetWindowStyle(_wnd) & WS_CHILD) && (::GetMenu(_wnd) != NULL)), GetWindowExStyle(_wnd))) return;
    ::SetWindowPos(_wnd, NULL, 0, 0, rc.right - rc.left, rc.bottom - rc.top, SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE);
  }
};

#undef GET_WND

class HelperWindow
  :public THelperWindow<HelperWindow>
{
public:
  HelperWindow() :m_hWnd(NULL), m_OldWndProc(::DefWindowProc), m_bSubclassed(false) {}

  HWND GetHWND() const { return m_hWnd; }

  operator HWND() const { return m_hWnd; }

  bool RegisterWindowClass()
  {
    WNDCLASS wc = { 0 };
    wc.style = GetClassStyle();
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hIcon = NULL;
    wc.lpfnWndProc = HelperWindow::__WndProc;
    wc.hInstance = GetInstance();
    wc.hCursor = ::LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = NULL;
    wc.lpszMenuName = NULL;
    wc.lpszClassName = GetWindowClassName();
    ATOM ret = ::RegisterClass(&wc);
    assert(ret != NULL || ::GetLastError() == ERROR_CLASS_ALREADY_EXISTS);
    return ret != NULL || ::GetLastError() == ERROR_CLASS_ALREADY_EXISTS;
  }

  bool RegisterSuperclass()
  {
    // Get the class information from an existing
    // window so we can subclass it later on...
    WNDCLASSEX wc = { 0 };
    wc.cbSize = sizeof(WNDCLASSEX);
    if (!::GetClassInfoEx(NULL, GetSuperClassName(), &wc)) {
      if (!::GetClassInfoEx(GetInstance(), GetSuperClassName(), &wc)) {
        assert(!"Unable to locate window class");
        return NULL;
      }
    }
    m_OldWndProc = wc.lpfnWndProc;
    wc.lpfnWndProc = HelperWindow::__ControlProc;
    wc.hInstance = GetInstance();
    wc.lpszClassName = GetWindowClassName();
    ATOM ret = ::RegisterClassEx(&wc);
    assert(ret != NULL || ::GetLastError() == ERROR_CLASS_ALREADY_EXISTS);
    return ret != NULL || ::GetLastError() == ERROR_CLASS_ALREADY_EXISTS;
  }

  HWND Create(HWND hwndParent, LPCTSTR pstrName, DWORD dwStyle, DWORD dwExStyle, const RECT rc, HMENU hMenu = NULL)
  {
    return Create(hwndParent, pstrName, dwStyle, dwExStyle, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, hMenu);
  }


  HWND Create(HWND hwndParent, LPCTSTR pstrName, DWORD dwStyle, DWORD dwExStyle, int x = CW_USEDEFAULT, int y = CW_USEDEFAULT, int cx = CW_USEDEFAULT, int cy = CW_USEDEFAULT, HMENU hMenu = NULL)
  {
    if (GetSuperClassName() != NULL && !RegisterSuperclass()) return NULL;
    if (GetSuperClassName() == NULL && !RegisterWindowClass()) return NULL;
    m_hWnd = ::CreateWindowEx(dwExStyle, GetWindowClassName(), pstrName, dwStyle, x, y, cx, cy, hwndParent, hMenu, GetInstance(), this);
    assert(m_hWnd != NULL);
    return m_hWnd;
  }

  HWND CreateHelperWindow(HWND hwndParent, LPCTSTR pstrWindowName, DWORD dwStyle = 0, DWORD dwExStyle = 0)
  {
    return Create(hwndParent, pstrWindowName, dwStyle, dwExStyle, 0, 0, 0, 0, NULL);
  }


  HWND Subclass(HWND hWnd)
  {
    assert(::IsWindow(hWnd));
    assert(m_hWnd == NULL);
    m_OldWndProc = SubclassWindow(hWnd, __WndProc);
    if (m_OldWndProc == NULL) return NULL;
    m_bSubclassed = true;
    m_hWnd = hWnd;
    ::SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LPARAM>(this));
    return m_hWnd;
  }


  void Unsubclass()
  {
    assert(::IsWindow(m_hWnd));
    if (!::IsWindow(m_hWnd)) return;
    if (!m_bSubclassed) return;
    SubclassWindow(m_hWnd, m_OldWndProc);
    m_OldWndProc = ::DefWindowProc;
    m_bSubclassed = false;
  }

  void ShowWindow(bool bShow = true, bool bTakeFocus = true)
  {
    ShowWindowHelper(bShow, bTakeFocus);
  }


  UINT ShowModal()
  {
    assert(::IsWindow(m_hWnd));
    UINT nRet = 0;
    HWND hWndParent = GetWindowOwner(m_hWnd);
    ::ShowWindow(m_hWnd, SW_SHOWNORMAL);
    ::EnableWindow(hWndParent, FALSE);
    MSG msg = { 0 };
    while (::IsWindow(m_hWnd) && ::GetMessage(&msg, NULL, 0, 0)) {
      if (msg.message == WM_CLOSE && msg.hwnd == m_hWnd) {
        nRet = msg.wParam;
        ::EnableWindow(hWndParent, TRUE);
        ::SetFocus(hWndParent);
      }
      ::TranslateMessage(&msg);
      ::DispatchMessage(&msg);

      if (msg.message == WM_QUIT) break;
    }
    ::EnableWindow(hWndParent, TRUE);
    ::SetFocus(hWndParent);
    if (msg.message == WM_QUIT) ::PostQuitMessage(msg.wParam);
    return nRet;
  }

  UINT ShowModal(bool bShow, bool bTakeFocus)
  {
    assert(::IsWindow(m_hWnd));
    UINT nRet = 0;
    HWND hWndParent = GetWindowOwner(m_hWnd);
    //::ShowWindow(m_hWnd, SW_SHOWNORMAL);
    ::ShowWindow(m_hWnd, bShow ? (bTakeFocus ? SW_SHOWNORMAL : SW_SHOWNOACTIVATE) : SW_HIDE);
    ::EnableWindow(hWndParent, FALSE);
    MSG msg = { 0 };
    while (::IsWindow(m_hWnd) && ::GetMessage(&msg, NULL, 0, 0)) {
      if (msg.message == WM_CLOSE && msg.hwnd == m_hWnd) {
        nRet = msg.wParam;
        ::EnableWindow(hWndParent, TRUE);
        ::SetFocus(hWndParent);
      }
      ::TranslateMessage(&msg);
      ::DispatchMessage(&msg);
      if (msg.message == WM_QUIT) break;
    }
    ::EnableWindow(hWndParent, TRUE);
    ::SetFocus(hWndParent);
    if (msg.message == WM_QUIT) ::PostQuitMessage(msg.wParam);
    return nRet;
  }


  void Close(UINT nRet = IDOK)
  {
    CloseHelper(nRet);
  }

  void CenterWindow()
  {
    CenterWindowHelper();
  }


  void SetIcon(UINT nRes)
  {
    //HICON hIcon = (HICON)::LoadImage(CPaintManagerUI::GetInstance(), MAKEINTRESOURCE(nRes), IMAGE_ICON, ::GetSystemMetrics(SM_CXICON), ::GetSystemMetrics(SM_CYICON), LR_DEFAULTCOLOR);
    HICON hIcon = (HICON)::LoadImage(GetInstance(), MAKEINTRESOURCE(nRes), IMAGE_ICON, (::GetSystemMetrics(SM_CXICON) + 15) & ~15, (::GetSystemMetrics(SM_CYICON) + 15) & ~15, LR_DEFAULTCOLOR);
    assert(hIcon);
    ::SendMessage(m_hWnd, WM_SETICON, (WPARAM)TRUE, (LPARAM)hIcon);
    //hIcon = (HICON)::LoadImage(CPaintManagerUI::GetInstance(), MAKEINTRESOURCE(nRes), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);
    hIcon = (HICON)::LoadImage(GetInstance(), MAKEINTRESOURCE(nRes), IMAGE_ICON, (::GetSystemMetrics(SM_CXSMICON) + 15) & ~15, (::GetSystemMetrics(SM_CYSMICON) + 15) & ~15, LR_DEFAULTCOLOR);
    assert(hIcon);
    ::SendMessage(m_hWnd, WM_SETICON, (WPARAM)FALSE, (LPARAM)hIcon);
  }

  LRESULT SendMessage(UINT uMsg, WPARAM wParam = 0, LPARAM lParam = 0L)
  {
    return SendMessageHelper(uMsg, wParam, lParam);
  }

  LRESULT PostMessage(UINT uMsg, WPARAM wParam = 0, LPARAM lParam = 0L)
  {
    return PostMessageHelper(uMsg, wParam, lParam);
  }

  void ResizeClient(int cx = -1, int cy = -1)
  {
    ResizeClientHelper(cx, cy);
  }

protected:
  virtual LPCTSTR GetWindowClassName() const = 0;
  virtual LPCTSTR GetSuperClassName() const { return NULL; }
  virtual UINT GetClassStyle() const { return CS_DBLCLKS; }

  virtual HINSTANCE GetInstance() { return NULL; }

  virtual LRESULT HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
  {
    bHandled = FALSE;
    return 0;
  }

  virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
  {
    LRESULT lRes = 0;
    BOOL bHandled = TRUE;
    lRes = HandleCustomMessage(uMsg, wParam, lParam, bHandled);
    if (bHandled) return lRes;

    return ::CallWindowProc(m_OldWndProc, m_hWnd, uMsg, wParam, lParam);
  }

  virtual void OnFinalMessage(HWND hWnd) {}

  static LRESULT CALLBACK __WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
  {
    HelperWindow* pThis = NULL;
    if (uMsg == WM_NCCREATE) {
      LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
      pThis = static_cast<HelperWindow*>(lpcs->lpCreateParams);
      pThis->m_hWnd = hWnd;
      ::SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LPARAM>(pThis));
    } else {
      pThis = reinterpret_cast<HelperWindow*>(::GetWindowLongPtr(hWnd, GWLP_USERDATA));
      if (uMsg == WM_NCDESTROY && pThis != NULL) {
        LRESULT lRes = ::CallWindowProc(pThis->m_OldWndProc, hWnd, uMsg, wParam, lParam);
        ::SetWindowLongPtr(pThis->m_hWnd, GWLP_USERDATA, 0L);
        if (pThis->m_bSubclassed) pThis->Unsubclass();
        pThis->m_hWnd = NULL;
        pThis->OnFinalMessage(hWnd);
        return lRes;
      }
    }
    if (pThis != NULL) {
      return pThis->HandleMessage(uMsg, wParam, lParam);
    } else {
      return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
  }
  static LRESULT CALLBACK __ControlProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
  {
    HelperWindow* pThis = NULL;
    if (uMsg == WM_NCCREATE) {
      LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
      pThis = static_cast<HelperWindow*>(lpcs->lpCreateParams);
      ::SetProp(hWnd, L"WndX", (HANDLE)pThis);
      pThis->m_hWnd = hWnd;
    } else {
      pThis = reinterpret_cast<HelperWindow*>(::GetProp(hWnd, L"WndX"));
      if (uMsg == WM_NCDESTROY && pThis != NULL) {
        LRESULT lRes = ::CallWindowProc(pThis->m_OldWndProc, hWnd, uMsg, wParam, lParam);
        if (pThis->m_bSubclassed) pThis->Unsubclass();
        ::SetProp(hWnd, L"WndX", NULL);
        pThis->m_hWnd = NULL;
        pThis->OnFinalMessage(hWnd);
        return lRes;
      }
    }
    if (pThis != NULL) {
      return pThis->HandleMessage(uMsg, wParam, lParam);
    } else {
      return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
  }

protected:
  HWND m_hWnd;
  WNDPROC m_OldWndProc;
  bool m_bSubclassed;
};

HelperLibEnd