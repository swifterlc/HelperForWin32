#pragma once
#include "common.h"
#include <functional>
#include <stack>

HelperLibBegin

class Defer
{
    using Task = std::function<void()>;
public:
    Defer& operator+=(Task const& task)
    {
        task_stack_.push(task);
        return *this;
    }

    virtual ~Defer()
    {
        task_stack_.top()();
        task_stack_.pop();
    }

private:
    std::stack<Task> task_stack_{};
};

#define USE_DEFER helper_lib::Defer __defer__;
#define defer __defer__ += 

HelperLibEnd