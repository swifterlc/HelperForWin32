#ifndef __UIOPTION_H__
#define __UIOPTION_H__

#pragma once

namespace DuiLib
{
	class DUILIB_API COptionUI : public CButtonUI
	{
	public:
		COptionUI();
		~COptionUI();

		LPCTSTR GetClass() const;
		LPVOID GetInterface(LPCTSTR pstrName);

		void SetManager(CPaintManagerUI* pManager, CControlUI* pParent, bool bInit = true);

		bool Activate();
		void SetEnabled(bool bEnable = true);

		LPCTSTR GetSelectedImage();
		void SetSelectedImage(LPCTSTR pStrImage);

		LPCTSTR GetSelectedHotImage();
		void SetSelectedHotImage(LPCTSTR pStrImage);

    // #ADDED:
    inline void SetSelectedPushedImage(LPCTSTR pStrImage) {if (m_diSelectedPushed.sDrawString == pStrImage && m_diSelectedPushed.pImageInfo != NULL) return; m_diSelectedPushed.Clear(); m_diSelectedPushed.sDrawString = pStrImage;Invalidate();}
    inline LPCTSTR GetSelectedPushedImage() {return m_diSelectedPushed.sDrawString;}
    inline bool IsDrawPushedImage() const { return m_isDrawPushedImage; }
    inline void SetDrawPushedImage(bool val) { m_isDrawPushedImage = val; }
    inline DWORD GetSelectedDisabledTextColor() const { return m_dwSelectedDisabledTextColor; }
    inline void SetSelectedDisabledTextColor(DWORD val) { m_dwSelectedDisabledTextColor = val; }
    inline LPCTSTR GetSelectedDisabledImage() const { return m_diSelectedDisabled.sDrawString; }
    inline void SetSelectedDisabledImage(LPCTSTR pStrImage) { if (m_diSelectedDisabled.sDrawString == pStrImage && m_diSelectedDisabled.pImageInfo != NULL) return; m_diSelectedDisabled.Clear(); m_diSelectedDisabled.sDrawString = pStrImage; Invalidate(); }

		void SetSelectedTextColor(DWORD dwTextColor);
		DWORD GetSelectedTextColor();

		void SetSelectedBkColor(DWORD dwBkColor);
		DWORD GetSelectBkColor();

		LPCTSTR GetForeImage();
		void SetForeImage(LPCTSTR pStrImage);

		LPCTSTR GetGroup() const;
		void SetGroup(LPCTSTR pStrGroupName = NULL);
		bool IsSelected() const;
		virtual void Selected(bool bSelected, bool bTriggerEvent=true);

		SIZE EstimateSize(SIZE szAvailable);
		void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);

		void PaintStatusImage(HDC hDC);
		void PaintText(HDC hDC);
  protected:
		bool			m_bSelected;
		CDuiString		m_sGroupName;

		DWORD			m_dwSelectedBkColor;
		DWORD			m_dwSelectedTextColor;

		TDrawInfo		m_diSelected;
		TDrawInfo		m_diSelectedHot;
    // #ADDED
    TDrawInfo   m_diSelectedPushed;
    bool  m_isDrawPushedImage;  //是否绘制按下状态的背景图 
    DWORD			m_dwSelectedDisabledTextColor = 0;  //选中状态不可用状态文字颜色
    TDrawInfo m_diSelectedDisabled; //按下状态不可用背景图

		TDrawInfo		m_diFore;
	};

} // namespace DuiLib

#endif // __UIOPTION_H__