#pragma once

namespace DuiLib
{
  class CPromptEditWnd;
  class DUILIB_API CPromptEditUI : public CLabelUI
  {
    friend class CPromptEditWnd;
  public:
    CPromptEditUI();

    LPCTSTR GetClass() const;
    LPVOID GetInterface(LPCTSTR pstrName);
    UINT GetControlFlags() const;
    HWND GetNativeWindow() const;

    void SetEnabled(bool bEnable = true);
    void SetText(LPCTSTR pstrText);
    void SetMaxChar(UINT uMax);
    UINT GetMaxChar();
    void SetReadOnly(bool bReadOnly);
    bool IsReadOnly() const;
    void SetPasswordMode(bool bPasswordMode);
    bool IsPasswordMode() const;
    void SetPasswordChar(TCHAR cPasswordChar);
    TCHAR GetPasswordChar() const;
    bool IsAutoSelAll();
    void SetAutoSelAll(bool bAutoSelAll);
    void SetNumberOnly(bool bNumberOnly);
    bool IsNumberOnly() const;
    int GetWindowStyls() const;
    HWND GetNativeEditHWND() const;

    LPCTSTR GetNormalImage();
    void SetNormalImage(LPCTSTR pStrImage);
    LPCTSTR GetHotImage();
    void SetHotImage(LPCTSTR pStrImage);
    LPCTSTR GetFocusedImage();
    void SetFocusedImage(LPCTSTR pStrImage);
    LPCTSTR GetDisabledImage();
    void SetDisabledImage(LPCTSTR pStrImage);
    void SetNativeEditBkColor(DWORD dwBkColor);
    DWORD GetNativeEditBkColor() const;

    void SetSel(long nStartChar, long nEndChar);
    void SetSelAll();
    void SetReplaceSel(LPCTSTR lpszReplace);

    void SetPos(RECT rc, bool bNeedInvalidate = true);
    void Move(SIZE szOffset, bool bNeedInvalidate = true);
    void SetVisible(bool bVisible = true);
    void SetInternVisible(bool bVisible = true);
    SIZE EstimateSize(SIZE szAvailable);
    void DoEvent(TEventUI& event);
    void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);

    void PaintStatusImage(HDC hDC);
    void PaintText(HDC hDC);

    inline void SetPromptText(CDuiString prompt) { m_prompt_text_ = prompt; }
    inline CDuiString GetPromptText() { return m_prompt_text_; }
    inline void SetPromptTextColor(DWORD color) { m_prompt_text_color_ = color; }
    inline DWORD GetPromptTextColor() { return m_prompt_text_color_; }
    inline void SetNormalTextColor(DWORD color) { m_normal_text_color_ = color; }
    inline DWORD GetNormalTextColor() { return m_normal_text_color_; }
    inline void SetNativeEditTextColor(DWORD dwTextColor) { m_native_text_color_ = dwTextColor; };
    inline DWORD GetNativeEditTextColor() { return m_native_text_color_; }
    inline CDuiString GetText() { return is_prompt_ ? _T("") : __super::GetText(); }

  protected:
    CPromptEditWnd * m_pWindow;

    UINT m_uMaxChar;
    bool m_bReadOnly;
    bool m_bPasswordMode;
    bool m_bAutoSelAll;
    TCHAR m_cPasswordChar;
    UINT m_uButtonState;
    DWORD m_dwEditbkColor;
    int m_iWindowStyls;

    TDrawInfo m_diNormal;
    TDrawInfo m_diHot;
    TDrawInfo m_diFocused;
    TDrawInfo m_diDisabled;


    //添加的属性
    DWORD m_native_text_color_;
    DWORD m_prompt_text_color_;
    DWORD m_normal_text_color_;
    CDuiString m_prompt_text_;

    bool is_prompt_;  //是否正在显示提示内容
  };

}