#pragma once

#pragma warning(push)
#pragma warning(disable:4596)
#pragma warning(disable:4091)
#include "UIlib.h"
using namespace DuiLib;
#undef GetFirstChild
#undef GetNextSibling
#include "easing_curve.h"
#include "uipatch.h"
#pragma warning(pop)