#pragma once

class Profile
{
public:
  Profile(LPCWSTR path)
    :path_(path)
  {
    profile_.load_file(path_.c_str());
  }

  ~Profile() = default;

  template<class T>
  T Get(LPCWSTR name, T def)
  {
    pugi::xml_node node = Node(name);
    // 属性不存在情况下，调用设置函数写入默认值
    if (!node.text()) {
      Set(name, def);
      return def;
    }

    return As(name, def);
  }

  template<class T>
  void Set(LPCWSTR name, T value)
  {
    Node(name).text().set(value);
    Save();
  }

  template<typename T>
  T GetAttr(LPCWSTR node_name, LPCWSTR attr_name, T def)
  {
    pugi::xml_node node = Node(node_name);
    // 属性不存在情况下，调用设置函数写入默认值
    pugi::xml_attribute attr = node.attribute(attr_name);
    if (!attr) {
      attr = node.append_attribute(attr_name);
      attr.set_value(def);
      Save();
    }
    return As(attr, def);
  }

  void Set(LPCWSTR name, const std::map<LPCWSTR, LPCWSTR>& props)
  {
    pugi::xml_node node = Node(name);
    for (const auto &i : props) {
      pugi::xml_attribute attr = node.attribute(i.first);
      if (!attr) attr = node.append_attribute(i.first);
      attr.set_value(i.second);
    }
    Save();
  }

  std::map<std::wstring, std::wstring> Get(LPCWSTR name)
  {
    std::map<std::wstring, std::wstring> props;
    pugi::xml_node node = Node(name);
    for (auto& i : node.attributes()) props[i.name()] = i.as_string();
    return std::move(props);
  }

protected:

  void Save()
  {
    if (path_.empty())
      return;
    profile_.save_file(path_.c_str(), L"  ", pugi::format_indent | pugi::format_write_bom, pugi::encoding_utf8);
  }

  template<class V>
  static bool SetAttr(pugi::xml_node &node, LPCWSTR name, V&& v)
  {
    pugi::xml_attribute attr = node.attribute(name);
    if (!attr) attr = node.append_attribute(name);
    return attr.set_value(std::forward<V>(v));
  }

  pugi::xml_node Node(LPCWSTR name)
  {
    pugi::xml_node root = profile_.child(root_name_.c_str());
    if (!root) {
      if (profile_.first_child().type() != pugi::node_declaration) {
        profile_.append_child(pugi::node_declaration).set_name(
          L"xml version=\"1.0\" encoding=\"utf-8\""
        );
      }

      root = profile_.append_child(root_name_.c_str());
      root.append_attribute(L"name") = L"default";
      root.append_attribute(L"version") = L"1.0";
    }
    pugi::xml_node node = root.child(name);
    if (!node) node = root.append_child(name);
    return node;
  }

  inline bool As(LPCWSTR name, bool def) { return Node(name).text().as_bool(def); }
  inline LPCWSTR As(LPCWSTR name, LPCWSTR def) { return Node(name).text().as_string(def); }
  inline double As(LPCWSTR name, double def) { return Node(name).text().as_double(def); }
  inline float As(LPCWSTR name, float def) { return Node(name).text().as_float(def); }
  inline int As(LPCWSTR name, int def) { return Node(name).text().as_int(def); }
  inline long long As(LPCWSTR name, long long def) { return Node(name).text().as_llong(def); }
  inline unsigned int As(LPCWSTR name, unsigned int def) { return Node(name).text().as_uint(def); }
  inline unsigned long long As(LPCWSTR name, unsigned long long def) { return Node(name).text().as_ullong(def); }

  template<typename T> inline bool As(T& t, bool def) { return t.as_bool(def); }
  template<typename T> inline LPCWSTR As(T& t, LPCWSTR def) { return t.as_string(def); }
  template<typename T> inline double As(T& t, double def) { return t.as_double(def); }
  template<typename T> inline float As(T& t, float def) { return t.as_float(def); }
  template<typename T> inline int As(T& t, int def) { return t.as_int(def); }
  template<typename T> inline long long As(T& t, long long def) { return t.as_llong(def); }
  template<typename T> inline unsigned int As(T& t, unsigned int def) { return t.as_uint(def); }
  template<typename T> inline unsigned long long As(T& t, unsigned long long def) { return t.as_ullong(def); }

private:
  std::wstring root_name_{ L"root" };
  const std::wstring path_;
  pugi::xml_document profile_;
};