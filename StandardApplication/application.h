#pragma once
#include "profile.h"

class MainFrame;
class Application
{
  Application();
  ~Application();
public:

  static Application* Instance();

  static void DestroyInstance();

  bool Initialize(HINSTANCE inst);

  bool Run(LPCWSTR strCmdLine);

  void Uninitialize();


  inline DWORD GetMainThreadId() const;
  inline BOOL GetIsMainThread() const;

  MainFrame* GetMainFrame() const;

  const std::wstring& GetDataStorageDir() const;
  const std::wstring& GetModuleDir() const;
protected:

private:
  const DWORD main_thread_id_;

  CComModule module_;
  std::wstring module_dir_;
  std::wstring data_storage_directory_;

  std::unique_ptr<MainFrame> main_frame_;
};
