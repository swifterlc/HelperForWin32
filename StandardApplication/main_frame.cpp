#include "stdafx.h"
#include "main_frame.h"

MainFrame::MainFrame()
{

}

void MainFrame::OnInitWnd()
{
  __super::OnInitWnd();


}

void MainFrame::OnFinalMessage(HWND hWnd)
{
  ::PostQuitMessage(0);
}

void MainFrame::Notify(TNotifyUI& msg)
{

}

LRESULT MainFrame::HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  if (uMsg == kAsyncTask) {
    helper_lib::ExecuteAsynTask(lParam);
    bHandled = true;
    return 0;
  }

  return __super::HandleCustomMessage(uMsg, wParam, lParam, bHandled);
}


